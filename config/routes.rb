Rails.application.routes.draw do
  resources :issues
   resources :issues do
    resources :comments
  end
  
  get 'sessions/create'

  get 'sessions/destroy'

  get 'home/show'

  root 'application#hello'
end

IssueTracker::Application.routes.draw do
  resources :issues
  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/home/show')
  get 'signout', to: 'sessions#destroy', as: 'signout'

  resources :sessions, only: [:create, :destroy]
  resource :home, only: [:show]

  root to: "home#show"
  
  post '/issues/:id/vote' => "issues#vote", as: :vote
  post '/issues/:id/watch' => "issues#watch", as: :watch
  put '/issues/:id/status' => "issues#update_status", as: :update_status
end