class AddVotesSeeings < ActiveRecord::Migration[5.1]
  def change
    add_column :issues, :votes, :integer, {default:0}
    add_column :issues, :watching, :integer, {default:0}
  end
end
