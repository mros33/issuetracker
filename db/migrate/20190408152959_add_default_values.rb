class AddDefaultValues < ActiveRecord::Migration[5.1]
  def change
    change_column :issues, :votes, :integer, {default:0}
    change_column :issues, :watching, :integer, {default:0}
  end
end
