class CreateIssues < ActiveRecord::Migration[5.1]
  def change
    create_table :issues do |t|
      t.string :title
      t.string :description
      t.string :tipus
      t.string :priority
      t.string :status
      t.string :user_id
      t.datetime :created_at
      t.datetime :updated_at
      t.string :assignee_id

      t.timestamps
    end
  end
end
