class User < ActiveRecord::Base
  
  #acts_as_token_authenticatable
  
  has_many :issues
  has_many :votes
  has_many :watchers
  has_many :comments, dependent: :destroy
  


  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.save!
      
        unless user
             user = User.create(name: data['name'],
                email: data['email'],
                password: Devise.friendly_token[0,20]
            )
        end
        user
    end
  end
end