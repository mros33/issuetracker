class Issue < ApplicationRecord
  belongs_to :user
  belongs_to :assignee, class_name: 'User', foreign_key: :assignee_id, optional: true
  has_many :votes, dependent: :destroy
  has_many :watchers, dependent: :destroy
  has_many :comments, dependent: :destroy
  validates :title, :tipus, :priority, :status, presence: true

 
  def self.tipus
  ["Bug", "Enhancement", "Proposal", "Task"]
  end
  def self.priority
  ["Trivial", "Minor", "Major", "Critical", "Blocker"]
  end
  def self.status
  ["New", "Open", "On hold", "Resolved", "Duplicate", "Invalid", "Won't fix", "Closed"]
  end
end
