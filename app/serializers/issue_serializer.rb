class IssueSerializer < ActiveModel::Serializer
 attributes :id, :title, :description, :tipus, :priority, :status, :created_at, :updated_at, :votes, :watchers

  def votes
    object.votes.size
  end
  def watchers
    object.watchers.size
  end
 
end
