class IssueIndexSerializer < ActiveModel::Serializer
 attribute :voted_by_current_user, if: :current_user?
 attribute :watched_by_current_user, if: :current_user?
 attributes :_links
 
  def current_user?
    true if current_user
  end

  def voted_by_current_user
    object.votes.exists?(current_user.id)
  end

  def watched_by_current_user
    object.watchers.exists?(current_user.id)
  end

  def _links
    if object.assignee != nil
      links = {
          self: { href: "/issues/#{object.id}" },
          creator: { href: "/users/#{object.user_id}", id: object.user_id, name: User.find(object.user_id).name },
          assignee: { href: "/users/#{object.assignee}", id: object.assignee, name: User.find(object.assignee).name },
      }
    else
      links = {
        self: { href: "/issues/#{object.id}" },
        creator: { href: "/users/#{object.user_id}", id: object.user_id, name: User.find(object.user_id).name },
        assignee: { href: nil, id: nil, name: nil },
      }
    end
  end
end
