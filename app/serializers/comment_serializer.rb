class CommentSerializer < ActiveModel::Serializer
  attributes :id, :body, :created_at, :updated_at, :_links
  
  def _links
      links = {
          self: { href: "/issues/#{object.issue_id}/comments/#{object.id}"},
          creator: { href: "/users/#{object.user_id}", name: User.find(object.user_id).name, id: object.user_id},
          issue: {href: "/issues/#{object.issue_id}"},
      }
  end
  
end
