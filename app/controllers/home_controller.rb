class HomeController < ApplicationController
  def show
    
    redirect_to "/issues" if current_user
    
  end
end
