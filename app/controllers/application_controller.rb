class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  helper_method :current_user
  skip_before_action :verify_authenticity_token, if: :json_request?
  
  private

  def json_request?
    request.format.json?
  end

  # Confirms a logged-in user.
  def logged_in_user
    unless logged_in?
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end

  #def current_user
   # @current_user ||= User.find(session[:user_id]) if session[:user_id]
  #end

  #   include ActionController::HttpAuthentication::Token::ControllerMethods
  # Add a before_action to authenticate all requests.
  # Move this to subclassed controllers if you only
  # want to authenticate certain methods.

  protected

  # Authenticate the user with token based authentication
  def authenticate
    unless authenticate_token || current_user
      if request.format.json?
        # self.headers["WWW-Authenticate"] = %(Token realm="#{realm}")
        render json: "{ 'result': 'error', 'message': 'Bad credentials' }", status: :unauthorized
      end
    end
  end

  def authenticate_token
    token = request.headers["X-User-Token"]
    if token.nil?
      false
    else
      @current_user = User.find_by(oauth_token: token)
      if @current_user.nil?
        false
      else
        true
      end
    end
  end

  def render_unauthorized(realm = "Application")
    response do |format|
      format.html {
        redirect_to login_path
      }
      format.json {
        self.headers["WWW-Authenticate"] = %(Token realm="#{realm}")
        render json: "{'result': 'error', message: 'Bad credentials'}", status: :unauthorized
      }
    end
  end
  
  public 
  
  def hello
    redirect_to "/home/show"
  end
  
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
end
