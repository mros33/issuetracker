class IssuesController < ApplicationController
  #acts_as_token_authentication_handler_for User
  before_action :authenticate
  before_action :set_issue, only: [:show, :edit, :update, :destroy]
  helper_method :sort_column, :sort_direction
  
  # GET /issues
  # GET /issues.json
  #def index
  #  redirect_to "/home/show" if not current_user
  #  @issues = Issue.all.order(sort_column + ' ' + sort_direction)
  
  def index
    respond_to do |format|  
      @issues = Issue.all.order(sort_column + ' ' + sort_direction)
      @issues = @issues.where(tipus: params[:tipus])  if params.has_key?(:tipus)
      @issues = @issues.where(priority: params[:priority]) if params.has_key?(:priority)
    
      if params.has_key?(:watcher)
          if User.exists?(id: params[:watcher])
            @issues = Issue.joins(:watchers).where(watchers:{user_id: params[:watcher]})
          end
      end
          
      if params.has_key?(:assignee)
        if User.exists?(id: params[:assignee])
          @issues = @issues.where(assignee_id: params[:assignee])
        end
      end
      
      if params.has_key?(:status)
        if params[:status] == "New&Open"
          @issues = @issues.where(status: ["Open","New"])
        else
        @issues = @issues.where(status: params[:status])
        end
      end
      format.html
      format.json {render json: @issues, status: :ok}
    end
    
  end

  # GET /issues/1
  # GET /issues/1.json
  def show
    respond_to do |format|
      format.html
      format.json {render json: @issue, status: :ok, serializer: IssueSerializer}
    end
  end

  # GET /issues/new
  def new
    @issue = Issue.new
  end

  # GET /issues/1/edit
  def edit
  end

  # POST /issues
  # POST /issues.json
  def create
    @issue = Issue.new(issue_params)
    @issue.created_at = Time.now
    @issue.updated_at = Time.now
    @issue.user_id = @current_user.id
    

    respond_to do |format|
      if @issue.save
        @watcher = Watcher.new
          @watcher.user_id = @current_user.id
          @watcher.issue_id = @issue.id
          @watcher.save
        format.html { redirect_to @issue, notice: 'Issue was successfully updated.' }
        format.json { render json: @issue, status: :created, serializer: IssueSerializer }
      else
        format.html { render :new }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /issues/1
  # PATCH/PUT /issues/1.json
  def update
    
    @issue = Issue.find(params[:id])
    if @issue.assignee_id != params[:assignee_id]
        @comment = Comment.new
        @comment.issue_id = @issue.id
        @comment.user_id = @current_user.id
        @comment.body =  " assigned issue to " + User.find(params[:assignee_id]).name
        @comment.save 
    end
    if @issue.status != params[:status]
        @comment = Comment.new
        @comment.issue_id = @issue.id
        @comment.user_id = current_user.id
        @comment.body =  " assigned issue to " + issue_params.status
        @comment.save 
    end
    respond_to do |format|
      if @issue.update(issue_params)
        
        format.html { redirect_to @issue, notice: 'Issue was successfully updated.' }
        format.json { render :show, status: :ok, location: @issue, serializer: ShowIssueSerializer }
      else
        format.html { render :edit }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update_status
    respond_to do |format|
      @issue_to_update = Issue.find(params[:id])
      @issue_to_update.update_attribute("status", params[:status])
       @comment = Comment.new
       @comment.issue_id = @issue_to_update.id
        @comment.user_id = @current_user.id
        @comment.body =  "changed status to " + @issue_to_update.status 
        @comment.save 
      format.html { redirect_to @issue_to_update }
      format.json { render json: @issue_to_update, status: :ok }
    end
  end


  # DELETE /issues/1
  # DELETE /issues/1.json
  def destroy
    @issue.destroy
    respond_to do |format|
      format.html { redirect_to issues_url, notice: 'Issue was successfully destroyed.' }
      format.json { render json: {message: 'Issue deleted successfully'}, status: :ok }
    end
  end
  
    # POST /issues/{issue_id}/watch
  
  def watch
    respond_to do |format|
      @issue_to_watch = Issue.find(params[:id])
      if !Watcher.exists?(:issue_id => @issue_to_watch.id, :user_id => @current_user.id)
        @watcher = Watcher.new
        @watcher.user_id = @current_user.id
        @watcher.issue_id = @issue_to_watch.id
        @watcher.save
      else
        @watcher = Watcher.where(issue_id: params[:id], user_id: @current_user.id).take
        @watcher.destroy
      end
      if params[:view] == "index"
        format.html { redirect_to issues_url}
      else
        format.html { redirect_to @issue_to_watch}
      end
      format.json { render json: @issue_to_watch, status: :ok }
    end
  end
  
  # POST /issues/{issue_id}/vote
  
  def vote
    respond_to do |format|
      @issue_to_vote = Issue.find(params[:id])
      if !Vote.exists?(:issue_id => @issue_to_vote.id, :user_id => @current_user.id)
        @vote = Vote.new
        @vote.user_id = @current_user.id
        @vote.issue_id = @issue_to_vote.id
        @vote.save
      else
        @vote = Vote.where(issue_id: params[:id], user_id: @current_user.id).take
        @vote.destroy
      end
      
      format.html { redirect_to @issue_to_vote }
      format.json { render json: @issue_to_vote, status: :ok }
     
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_issue
      @issue = Issue.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def issue_params
      params.require(:issue).permit(:title, :description, :tipus, :priority, :status, :user_id, :created_at, :updated_at, :assignee_id, :watching, :votes)
    end
    
    def sort_column
      Issue.column_names.include?(params[:sort]) ? params[:sort] : "title"
    end
    
    def sort_direction
      %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
    end
end
